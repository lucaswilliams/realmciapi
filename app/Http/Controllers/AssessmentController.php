<?php

namespace App\Http\Controllers;

use App\Models\Assessment;
use App\Models\User;
use App\Models\UserAssessment;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AssessmentController extends Controller
{
    /**
     * Accepts JSON that contains a user, assessment info and question info.
     * @param Request $request
     */
    public function storeData(Request $request) {
        try {
            $content = $request->getContent();
            $json = json_decode($content);

            $dateOfParticipation = substr($json->DateOfParticipation, 6, 4).'-'.substr($json->DateOfParticipation, 3, 2).'-'.substr($json->DateOfParticipation, 0, 2);

            //Can we find a matching user?  Do we need a unique ID?  Database constraint is in place.
            $user = new User();
            $user->name = $json->Name;
            $user->participationCode = $json->ParticipationCode;
            $user->dateOfParticipation = $dateOfParticipation;
            $user->handover = $json->Handover != null && count($json->Handover) > 0 ? implode(', ', $json->Handover) : null;
            $user->audioFileName = $json->AudioFileName;
            $user->scenario = $json->Scenario;
            $user->failure = $json->Failure;
            $user->save();

            foreach ($json->Assesments as $jsonAssessment) {
                $assessment = new Assessment();
                $assessment->assessmentId = $jsonAssessment->Id;
                $assessment->name = $jsonAssessment->Name;
                $assessment->pointsEarned = $jsonAssessment->PointedEarned;
                $assessment->user_id = $user->id;
                $assessment->save();
            }
        } catch (\Exception $ex) {
            Log::error('There was a problem saving data: '.$ex->getMessage());
            $msg = 'Error';
            preg_match_all('/Duplicate entry \'(.*)\' for key \'(.*)\'/', $ex->getMessage(), $matches);
            if(is_array($matches)) {
                $msg = 'Error: '.$matches[0][0];
            }
            return json_encode(['status' => $msg]);
        }

        return json_encode(['status' => 'OK']);
    }

    public function downloadOne(Request $request) {
        $users = DB::table('users')
            ->where('participationCode', $request->my_data)
            ->get();
        if(count($users) == 0) {
            abort(404);
        }

        $data = [$this->getAssessmentsForUser($users[0])];

        //echo json_encode($data); die();

        return $this->provideDownload($data, 'participation-data-'.$request->my_data.'-'.date('Y-m-d-H-i-s'));
    }

    public function downloadAll(Request $request) {
        //ICgr4ntAcc355
        if(strtoupper(md5($request->all_data)) != 'D0C43B7C0A6B80EF48D7838826924DA0') {
            abort(403);
        }

        $data = [];
        $users = DB::table('users')
            ->get();

        foreach($users as $user) {
            $data[] = $this->getAssessmentsForUser($user);
        }
        return $this->provideDownload($data, 'participation-data-all-'.date('Y-m-d-H-i-s'));
    }

    private function getAssessmentsForUser($user) {
        $assessments = Assessment::where('user_id', $user->id)->get();
        $user->Assessments = $assessments;
        return $user;
    }

    private function provideDownload($data, $filename) {
        //Just display it for the moment
        $rows = [];
        $assessments = [];
        foreach($data as $person) {
            $row = new \stdClass();
            $row->id = $person->id;
            $row->name = $person->name;
            $row->participationCode = $person->participationCode;
            $row->audioFileName = $person->audioFileName;
            $row->handover = $person->handover;
            $row->dateOfParticipation = $person->dateOfParticipation;
            $row->scenario = $person->scenario;
            $row->failure = $person->failure;
            $row->assessments = [];

            foreach($person->Assessments as $assessment) {
                //echo '<pre>'; var_dump($assessment); echo '</pre>';
                $row->assessments[$assessment->name] = $assessment->pointsEarned;
                if(!in_array($assessment->name, $assessments)) {
                    $assessments[] = $assessment->name;
                }
            }
            $rows[] = $row;
        }

        $csv = [];
        $newrow = [
            'id',
            'name',
            'participationcode',
            'audiofilename',
            'handover',
            'dateofparticipation',
            'scenario',
            'failure'
        ];

        foreach($assessments as $assessment) {
            $newrow[] = $assessment;
        }

        $csv[] = $newrow;
        foreach($rows as $row) {
            $newrow = [
                $row->id,
                $row->name,
                $row->participationCode,
                $row->audioFileName,
                $row->handover,
                $row->dateOfParticipation,
                $row->scenario,
                $row->failure
            ];

            foreach($assessments as $assessment) {
                if(isset($row->assessments[$assessment])) {
                    $newrow[] = $row->assessments[$assessment];
                } else {
                    $newrow[] = '';
                }
            }

            $csv[] = $newrow;
        }

        //echo '<pre>'; var_dump($csv); echo '</pre>';

        $filename.='.csv';
        $temp = 'data.csv';
        $handle = fopen($temp, 'w+');
        foreach($csv as $line) {
            fputcsv($handle, $line);
        }
        fclose($handle);
        $headers = array('Content-type'=> 'text/csv');
        return response()->download($temp,$filename,$headers);
    }
}

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="https://portal.realresponse.com.au/css/bootstrap.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://portal.realresponse.com.au/css/custom.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
    <link href="https://select2.github.io/select2-bootstrap-theme/css/select2-bootstrap.css" rel="stylesheet" />

    <style>
        #cards {
            margin-top: 100px;
        }
    </style>

    <title>RealMCI Website</title>
</head>
<body>
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="https://portal.realresponse.com.au">
                <img src="https://portal.realresponse.com.au/img/realresponse.png">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    <!--<li class="nav-item">
    <a class="nav-link" href="https://portal.realresponse.com.au/login">Login</a>
    </li>-->
                </ul>
            </div>
        </div>
    </nav>
    <div class="container" id="cards">
        <div class="row justify-content-center">
            <div class="col-12 col-md-5">
                <div class="card">
                    <div class="card-body">
                        <form method="POST" action="download/one">
                            {{csrf_field()}}
                            <h1>Download your data</h1>
                            <div class="form-group">
                                <label for="my_data">Enter your participation code:</label>
                                <input type="text" class="form-control" id="my_data" name="my_data">
                            </div>
                            <button type="submit" class="btn btn-primary">Download my data</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-1 d-none d-md-flex"></div>
            <div class="col-12 col-md-5">
                <div class="card">
                    <div class="card-body">
                        <form method="POST" action="download/all">
                            {{csrf_field()}}
                            <h1>Download all participant data</h1>
                            <div class="form-group">
                                <label for="all_data">Enter the master password:</label>
                                <input type="password" class="form-control" id="all_data" name="all_data">
                            </div>
                            <button type="submit" class="btn btn-primary">Download all data</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="https://portal.realresponse.com.au/js/jquery.min.js"></script>
    <script type="text/javascript" src="https://portal.realresponse.com.au/js/popper.min.js"></script>
    <script type="text/javascript" src="https://portal.realresponse.com.au/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://portal.realresponse.com.au/js/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
    <script type="text/javascript" src="https://portal.realresponse.com.au/js/app.js"></script>

    <script type="text/javascript">
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
</body>
</html>

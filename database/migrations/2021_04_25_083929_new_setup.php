<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NewSetup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function(Blueprint $table) {
            $table->id('id');
            $table->string('name', 100);
            $table->string('participationCode', 5);
            $table->string('audioFileName', 100)->nullable();
            $table->longText('handover')->nullable();
            $table->date('dateOfParticipation')->nullable();
            $table->string('scenario', 50)->nullable();
            $table->longtext('failure')->nullable();
        });

		Schema::create('assessments', function(Blueprint $table) {
		    $table->integer('assessmentId');
		    $table->string('name', 100);
		    $table->integer('pointsEarned');
		    $table->foreignId('user_id')->constrained('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('assessments');
        Schema::drop('users');
    }
}
